package libnmdc

type UserFlag byte

const (
	FLAG_NORMAL          UserFlag = 1
	FLAG_AWAY_1          UserFlag = 2
	FLAG_AWAY_2          UserFlag = 3
	FLAG_SERVER_1        UserFlag = 4
	FLAG_SERVER_2        UserFlag = 5
	FLAG_SERVER_AWAY_1   UserFlag = 6
	FLAG_SERVER_AWAY_2   UserFlag = 7
	FLAG_FIREBALL_1      UserFlag = 8
	FLAG_FIREBALL_2      UserFlag = 9
	FLAG_FIREBALL_AWAY_1 UserFlag = 10
	FLAG_FIREBALL_AWAY_2 UserFlag = 11
)
