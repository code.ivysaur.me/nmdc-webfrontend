package main

type Config struct {
	App struct {
		MotdHTML        string `json:"motd"`
		ContentedServer string `json:"contented_server"`
	}

	Web struct {
		Port          int    `json:"port"`
		BindTo        string `json:"bind_to"`
		Title         string `json:"title"`
		CustomFavicon bool   `json:"custom_favicon"`

		ExternalWebroot bool `json:"external_webroot,omitempty"`
	}

	Hub struct {
		Address string `json:"address"`
		Port    int    `json:"port,omitempty"`
		Tag     string `json:"tag"`
	}
}
