# nmdc-webfrontend

A web interface to an NMDC/ADC hub.

Chat in real-time on your NMDC/ADC hub using a web browser.

## Relationship to other projects

This project forks and deprecates my earlier `dcwebui2` project since Go seems to use less memory than node.

This project supercedes [dcwebui](https://code.ivysaur.me/dcwebui/), [flexdc](https://code.ivysaur.me/flexdc/), and [dcwebui2](https://code.ivysaur.me/dcwebui2/).

### Upgrading from `dcwebui2`

- The configuration file content is identical between `nmdc-webfrontend` 1.0.0 and `dcwebui2` 1.3.0, but please now ensure it's valid JSON instead of arbitrary javascript. This means no assignment, use double-quoted strings, and no comments.
- Future changes to the configuration file since `nmdc-webfrontend` 1.0.0 are backward compatible (see the changelog for more details).

## Developing

1. Install Go (>= 1.11), Node.js, NPM, and 7-Zip (`p7zip-full` on Debian)
2. Download the source code
	- `git clone https://git.ivysaur.me/code.ivysaur.me/nmdc-webfrontend.git `; or
	- `go get code.ivysaur.me/nmdc-webfrontend` ; or
	- download and extract a source archive
3. Install dependencies: `sudo make deps`
4. Build: `make`
5. Optional: Set `web.external_webroot: true` in the config file for unminified development

## Changelog

2017-11-26 1.3.0
- Update libnmdc to 0.16 (adds ADC hub support)
- Configuration: The `hub.port` property is now optional. You can specify a full URI in the `hub.address` property instead.
- [⬇️ nmdc-webfrontend-1.3.0-win32.7z](https://git.ivysaur.me/attachments/76e6b425-d34d-4451-815b-98a6e0685a19) *(1.44  MiB)*
- [⬇️ nmdc-webfrontend-1.3.0-src.tar.xz](https://git.ivysaur.me/attachments/a362dd86-2999-4753-adac-830f1a1a64ac) *(64.81  KiB)*
- [⬇️ nmdc-webfrontend-1.3.0-linux64.tar.xz](https://git.ivysaur.me/attachments/877fc7c9-0df8-4514-9147-7f102f221d49) *(1.69  MiB)*

2017-11-14 1.2.3
- Update libnmdc to 0.15
- [⬇️ nmdc-webfrontend-1.2.3-win32.7z](https://git.ivysaur.me/attachments/02c33e38-89ef-4cbd-b8a2-80cbd6077520) *(1.41 MiB)*
- [⬇️ nmdc-webfrontend-1.2.3-src.tar.xz](https://git.ivysaur.me/attachments/e3e7599d-4805-4e39-a45e-d7f68cc32f09) *(64.72 KiB)*
- [⬇️ nmdc-webfrontend-1.2.3-linux64.tar.xz](https://git.ivysaur.me/attachments/c5a06c20-e14a-4cfd-b830-7b41a52074c1) *(1.66 MiB)*

2017-10-28 1.2.2
- Enhancement: Simplify build process
- Fix an issue with closing PM tabs
- [⬇️ nmdc-webfrontend-1.2.2-win32.7z](https://git.ivysaur.me/attachments/37f3a7f4-637b-44d7-9575-5a721a504ce5) *(1.41 MiB)*
- [⬇️ nmdc-webfrontend-1.2.2-src.tar.xz](https://git.ivysaur.me/attachments/265ed79c-901f-43e7-9746-99b45a28311b) *(64.72 KiB)*
- [⬇️ nmdc-webfrontend-1.2.2-linux64.tar.xz](https://git.ivysaur.me/attachments/74ebeaaa-a2ef-4b9c-948f-8c11522d5ab0) *(1.66 MiB)*

2017-10-16 1.2.1
- Enhancement: Increase scrollback buffer size
- Fix an issue with missing `contented` upload link once logged in
- Fix a cosmetic issue with `(0)` appearing in page title
- [⬇️ nmdc-webfrontend-1.2.1-win32.7z](https://git.ivysaur.me/attachments/a99d5d16-968f-4b02-954a-e22314ae9d64) *(1.39 MiB)*
- [⬇️ nmdc-webfrontend-1.2.1-src.tar.xz](https://git.ivysaur.me/attachments/e9c3dd9f-c0b1-4913-8315-3a39c809155c) *(65.74 KiB)*
- [⬇️ nmdc-webfrontend-1.2.1-linux64.tar.xz](https://git.ivysaur.me/attachments/bf3dc821-182b-4911-a46f-72efbc4310f8) *(1.64 MiB)*

2017-10-15 1.2.0
- Feature: Add `contented` integration (set `app.contented_server` in config file)
- Fix a cosmetic issue with the menu icon on devices without unicode font coverage
- [⬇️ nmdc-webfrontend-1.2.0-win32.7z](https://git.ivysaur.me/attachments/48c194e1-6463-4449-a8d4-e2b1cef2a522) *(1.39 MiB)*
- [⬇️ nmdc-webfrontend-1.2.0-src.tar.xz](https://git.ivysaur.me/attachments/ceba8731-b0e6-483e-8282-360a0a584304) *(65.69 KiB)*
- [⬇️ nmdc-webfrontend-1.2.0-linux64.tar.xz](https://git.ivysaur.me/attachments/6082cb13-0e62-4fa8-95c6-1b7d7f7a3272) *(1.64 MiB)*

2017-02-11 1.1.4
- Update libnmdc to 0.14
- [⬇️ nmdc-webfrontend-1.1.4-win64.7z](https://git.ivysaur.me/attachments/b104b02c-9469-4b8d-be9f-3fbafecb4edc) *(1.36 MiB)*
- [⬇️ nmdc-webfrontend-1.1.4-win32.7z](https://git.ivysaur.me/attachments/dd0a9bab-6da6-45d4-a48f-51c9621e9c55) *(1.25 MiB)*
- [⬇️ nmdc-webfrontend-1.1.4-src.tar.xz](https://git.ivysaur.me/attachments/cc0be981-bad7-401e-aace-ac6d8d9a56dd) *(65.53 KiB)*
- [⬇️ nmdc-webfrontend-1.1.4-linux64.tar.xz](https://git.ivysaur.me/attachments/7cd26e52-9ea7-4dcc-91b3-273bc0a3eda4) *(1.50 MiB)*
- [⬇️ nmdc-webfrontend-1.1.4-linux32.tar.xz](https://git.ivysaur.me/attachments/15895b23-a582-4d59-91da-36f6e6145dfe) *(1.41 MiB)*

2017-02-11 1.1.3
- Feature: Display user IP address on hover, if available
- Enhancement: Allow clicking on popup notifications
- Enhancement: Only show 'popups enabled' notification when enabling for the first time, not persistent page load
- Update libnmdc to 0.13
- Fix a cosmetic issue with not displaying non-zero user share sizes
- [⬇️ nmdc-webfrontend-1.1.3-win64.7z](https://git.ivysaur.me/attachments/d7b551fb-0dd4-4c38-bbe2-513c67311752) *(1.36 MiB)*
- [⬇️ nmdc-webfrontend-1.1.3-win32.7z](https://git.ivysaur.me/attachments/33b90056-bff3-488c-bf01-bcaf7f114176) *(1.25 MiB)*
- [⬇️ nmdc-webfrontend-1.1.3-src.tar.xz](https://git.ivysaur.me/attachments/0390f266-3663-48b5-9216-4a3e41623017) *(65.50 KiB)*
- [⬇️ nmdc-webfrontend-1.1.3-linux64.tar.xz](https://git.ivysaur.me/attachments/8ab5ac7c-e8d2-47c0-886a-90bfaf011efd) *(1.50 MiB)*
- [⬇️ nmdc-webfrontend-1.1.3-linux32.tar.xz](https://git.ivysaur.me/attachments/229ef410-19a1-4fa8-8625-473bf486b40f) *(1.41 MiB)*

2017-02-06 1.1.2
- Autodetect 'extern', no need to include it in config files
- Enhancement: Remove redundant request, for a faster page load
- Display server version number in log file and in response headers
- Fix a cosmetic issue with spacing around user count in page title
- [⬇️ nmdc-webfrontend-1.1.2-win64.7z](https://git.ivysaur.me/attachments/96c0305e-dc3e-45c6-9567-5eaa461551ec) *(1.36 MiB)*
- [⬇️ nmdc-webfrontend-1.1.2-win32.7z](https://git.ivysaur.me/attachments/27095754-735c-4e24-8cff-60bdbbe60b34) *(1.25 MiB)*
- [⬇️ nmdc-webfrontend-1.1.2-src.tar.xz](https://git.ivysaur.me/attachments/5b9d177e-f79d-4afd-a97c-6d563ba0e7af) *(65.42 KiB)*
- [⬇️ nmdc-webfrontend-1.1.2-linux64.tar.xz](https://git.ivysaur.me/attachments/a2325dc5-992e-4d45-a622-c516e41a3187) *(1.50 MiB)*
- [⬇️ nmdc-webfrontend-1.1.2-linux32.tar.xz](https://git.ivysaur.me/attachments/e2dee266-1b9d-4837-9536-98b1922eefa0) *(1.41 MiB)*

2017-02-06 1.1.1
- Fix an issue with malformed content in minified build
- [⬇️ nmdc-webfrontend-1.1.1-win64.7z](https://git.ivysaur.me/attachments/412f04e2-08f5-4a12-b933-43ea00164fb2) *(1.36 MiB)*
- [⬇️ nmdc-webfrontend-1.1.1-win32.7z](https://git.ivysaur.me/attachments/52753dca-3dd6-4668-a785-2d5807d27e63) *(1.25 MiB)*
- [⬇️ nmdc-webfrontend-1.1.1-src.tar.xz](https://git.ivysaur.me/attachments/e9beee17-8368-4d21-b33f-9e3fa2e11c74) *(65.39 KiB)*
- [⬇️ nmdc-webfrontend-1.1.1-linux64.tar.xz](https://git.ivysaur.me/attachments/e93a09fc-7d0c-4b2a-a706-dddd51d9426d) *(1.50 MiB)*
- [⬇️ nmdc-webfrontend-1.1.1-linux32.tar.xz](https://git.ivysaur.me/attachments/bb9daa45-1bdb-492d-9d55-5fc39a046433) *(1.41 MiB)*

2017-02-06 1.1.0
- Feature: Remember last username/password for login; remember last "show joins/parts" status
- Feature: Display user details on hover (description, email, client tag, share size)
- Feature: Optional desktop notifications for background PMs (not possible in incognito)
- Feature: Automatically reconnect with the same username/password if connection was lost
- Feature: Re-enter last message (Ctrl+Up, Ctrl+Down)
- Feature: Set custom date/time format (Minutes, Seconds, Full), remembered for next session
- Feature: Clickable magnet links
- Feature: Display unread main-chat message count in the page title if the window is inactive
- Feature: Add warning message when closing tab while still connected (optional preference, disabled by default, will be remembered).
- Feature: Admin option to load a custom favicon (set `web.custom_favicon=true` and place a `favicon.ico` in the current directory)
- Feature: Admin option to use external web resources (set `web.external_webroot=true` and use the /client/ directory)
- Enhancement: Higher resolution favicon
- Enhancement: Display operators in green in the user list
- Enhancement: Enable spellcheck for text input once logged in
- Enhancement: Prevent sending referrer to remote URLs
- Enhancement: Display joins/parts and connection/disconnection messages in PM tabs
- Enhancement: Support Shift+Tab to autocomplete backward
- Enhancement: Support unread status for the main tab
- Enhancement: Improve page load time via minification
- Remove unused options from the config file
- Update socket.io to 1.7.2
- Update libnmdc to 0.12
- Add margin between bottom of the text area and the text input box
- Fix a cosmetic issue with collapsing consecutive spaces in posted messages
- Fix a cosmetic issue with text size adjustment on mobile devices
- Fix a cosmetic issue with clearing the screen on reconnection
- Fix a cosmetic issue with not clearing the userlist on certain types of network error
- Fix a cosmetic issue with closed PM tabs reappearing in some cases
- Fix a cosmetic issue with marking all PM tabs as read when switching to a single one
- [⬇️ nmdc-webfrontend-1.1.0-win64.7z](https://git.ivysaur.me/attachments/a88be3f1-a20f-4ae8-8723-fb538e321a73) *(1.36 MiB)*
- [⬇️ nmdc-webfrontend-1.1.0-win32.7z](https://git.ivysaur.me/attachments/09326166-ef9e-4901-91f9-ad3b0fff156f) *(1.25 MiB)*
- [⬇️ nmdc-webfrontend-1.1.0-src.tar.xz](https://git.ivysaur.me/attachments/6d83cf30-3436-4c5d-a6e4-715703af7ce7) *(65.39 KiB)*
- [⬇️ nmdc-webfrontend-1.1.0-linux64.tar.xz](https://git.ivysaur.me/attachments/a9baf26c-1756-49d9-a8aa-720f01d72194) *(1.50 MiB)*
- [⬇️ nmdc-webfrontend-1.1.0-linux32.tar.xz](https://git.ivysaur.me/attachments/00e5a0a2-4b3f-4276-aefe-56046707a14b) *(1.41 MiB)*

2016-11-29 1.0.2
- Rebuild with libnmdc 0.11
- Fix an issue with not setting a version in the client tag
- [⬇️ nmdc-webfrontend-1.0.2-win64.7z](https://git.ivysaur.me/attachments/6b84047d-9a4c-45fe-985b-5321f629f806) *(1.34 MiB)*
- [⬇️ nmdc-webfrontend-1.0.2-win32.7z](https://git.ivysaur.me/attachments/1505b8fa-83e1-469d-8c77-acee3711e26f) *(1.23 MiB)*
- [⬇️ nmdc-webfrontend-1.0.2-src.tar.xz](https://git.ivysaur.me/attachments/faff2112-d191-4ee8-a572-553bc4a25be8) *(54.28 KiB)*
- [⬇️ nmdc-webfrontend-1.0.2-linux64.tar.xz](https://git.ivysaur.me/attachments/c319c140-8bed-4225-a953-e99940ba9732) *(1.48 MiB)*
- [⬇️ nmdc-webfrontend-1.0.2-linux32.tar.xz](https://git.ivysaur.me/attachments/a55fc471-5d80-49ad-8670-67cfb8c6175c) *(1.39 MiB)*

2016-10-08 1.0.1
- Fix an issue with backward compatibility with `dcwebui2` configuration file format
- [⬇️ nmdc-webfrontend-1.0.1-win64.7z](https://git.ivysaur.me/attachments/d5ed658d-1ef3-4bd8-b2de-3d9415322f57) *(1.35 MiB)*
- [⬇️ nmdc-webfrontend-1.0.1-win32.7z](https://git.ivysaur.me/attachments/546bd5f9-3742-498f-a1cb-85690c84ddc2) *(1.23 MiB)*
- [⬇️ nmdc-webfrontend-1.0.1-src.tar.xz](https://git.ivysaur.me/attachments/5ec729d7-d914-4b7e-ade3-21988c203652) *(54.25 KiB)*
- [⬇️ nmdc-webfrontend-1.0.1-linux64.tar.xz](https://git.ivysaur.me/attachments/d7bf1c9c-ab2c-4c61-b7d8-7b6233ae059f) *(1.48 MiB)*
- [⬇️ nmdc-webfrontend-1.0.1-linux32.tar.xz](https://git.ivysaur.me/attachments/d5265319-81e5-4f3d-95f7-a6e46c607490) *(1.39 MiB)*

2016-10-08 1.0.0
- Port `dcwebui2` from Node.js (Javascript) to Go
- Fix a cosmetic issue with not clearing userlist on disconnection
- [⬇️ nmdc-webfrontend-1.0.0-win64.7z](https://git.ivysaur.me/attachments/3b7386c8-1445-4590-9779-af9870ea542a) *(1.34 MiB)*
- [⬇️ nmdc-webfrontend-1.0.0-win32.7z](https://git.ivysaur.me/attachments/9ad9bd21-4152-462c-9ddc-b584917058e5) *(1.23 MiB)*
- [⬇️ nmdc-webfrontend-1.0.0-src.tar.xz](https://git.ivysaur.me/attachments/d2745543-671c-42b5-b334-2dba1ee8726c) *(54.23 KiB)*
- [⬇️ nmdc-webfrontend-1.0.0-linux64.tar.xz](https://git.ivysaur.me/attachments/6102edf8-471b-4793-8877-c747f5ac5680) *(1.48 MiB)*
- [⬇️ nmdc-webfrontend-1.0.0-linux32.tar.xz](https://git.ivysaur.me/attachments/5a4bcb39-41e4-4db9-80ff-c0ea04912b06) *(1.39 MiB)*
