# Makefile for nmdc-webfrontend

BINNAME=nmdc-webfrontend

VERSION=1.3.1

GOFLAGS=-a \
	-ldflags "-s -w -X main.VERSION=$(BINNAME)/$(VERSION)" \
	-gcflags "-trimpath ${GOPATH}" \
	-asmflags "-trimpath ${GOPATH}"
	
SOURCES=client/ go.mod go.sum Makefile Config.go main.go nmdc-webfrontend.conf.SAMPLE

.PHONY: all deps clean

all: $(BINNAME)-$(VERSION)-win32.7z $(BINNAME)-$(VERSION)-linux64.tar.xz $(BINNAME)-$(VERSION)-src.tar.xz

deps:
	npm install -g less uglify-js less-plugin-clean-css html-minifier
	go get -u github.com/jteeuwen/go-bindata/...
	
clean:
	rm -f ./$(BINNAME)
	rm -f ./$(BINNAME).exe
	rm -fr ./clientpack
	rm -f ./bindata.go

bindata.go: client client/*
	rm -fr ./clientpack
	cp -r ./client ./clientpack
	( echo ';(function() {' ; cat clientpack/dcwebui.js ; echo '})();' ) | uglifyjs -o clientpack/dcwebui.min.js -c -m --ie8
	lessc --clean-css clientpack/dcwebui.css clientpack/dcwebui.min.css
	cat clientpack/index.htm \
		| sed -e '/dcwebui.css/{i <style>' -e 'r clientpack/dcwebui.min.css' -e 'a </style>' -e 'd}' \
		| sed -e '/dcwebui.js/{i <script>' -e 'r clientpack/dcwebui.min.js' -e 'a </script>' -e 'd}' \
		| sed -e '/socket.io-1.7.2.js/{i <script>' -e 'r clientpack/socket.io-1.7.2.js' -e 'a </script>' -e 'd}' \
		> clientpack/index.packed.htm
	mv clientpack/index.packed.htm clientpack/index.htm
	html-minifier --collapse-whitespace -o clientpack/index.min.htm clientpack/index.htm
	mv clientpack/index.min.htm clientpack/index.htm
	rm ./clientpack/*.js
	rm ./clientpack/*.css
	go-bindata -nomemcopy -nometadata -prefix clientpack clientpack
	
$(BINNAME).exe: bindata.go *.go
	GOARCH=386 GOOS=windows go build $(GOFLAGS) -o $(BINNAME).exe

$(BINNAME): bindata.go *.go
	GOARCH=amd64 GOOS=linux go build $(GOFLAGS) -o $(BINNAME)
	
$(BINNAME)-$(VERSION)-win32.7z: $(BINNAME).exe nmdc-webfrontend.conf.SAMPLE
	7z a -mx9 "$(BINNAME)-$(VERSION)-win32.7z" $(BINNAME).exe nmdc-webfrontend.conf.SAMPLE
	
$(BINNAME)-$(VERSION)-linux64.tar.xz: $(BINNAME) nmdc-webfrontend.conf.SAMPLE
	XZ_OPT='-9' tar caf "$(BINNAME)-$(VERSION)-linux64.tar.xz" $(BINNAME) nmdc-webfrontend.conf.SAMPLE --owner=0 --group=0

$(BINNAME)-$(VERSION)-src.tar.xz: $(SOURCES)
	XZ_OPT='-9' tar caf "$(BINNAME)-$(VERSION)-src.tar.xz" $(SOURCES) --owner=0 --group=0
