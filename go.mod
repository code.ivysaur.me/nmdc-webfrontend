module code.ivysaur.me/nmdc-webfrontend

require (
	code.ivysaur.me/libnmdc v0.16.0
	github.com/cxmcc/tiger v0.0.0-20170524142333-bde35e2713d7 // indirect
	github.com/googollee/go-engine.io v0.0.0-20170224222511-80ae0e43aca1 // indirect
	github.com/googollee/go-socket.io v0.0.0-20170525141029-5447e71f36d3
	github.com/gorilla/websocket v1.2.0 // indirect
)
